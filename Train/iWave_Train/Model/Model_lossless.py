import torch
import torch.optim as optim
from torch.autograd import Variable
import math
import sys
from torch.nn import functional as F
import numpy as np

import Model.PixelCNN_lossless as PixelCNN
import Model.learn_wavelet_trans_lossless as learn_wavelet_trans

import Model.core as core


class Model(torch.nn.Module):
    def __init__(self, trainable_set, check_step=4, shuffle_factor=2):
        super(Model, self).__init__()

        self.trans_steps = 4
        self.trainable_set = trainable_set
        self.wavelet_transform = torch.nn.ModuleList(
            learn_wavelet_trans.Wavelet(self.trainable_set) for _i in range(self.trans_steps))

        n = self.trans_steps
        self.coding_LL = PixelCNN.PixelCNN()
        self.coding_HL_list = torch.nn.ModuleList(
            [PixelCNN.PixelCNN_Context(10, 2), PixelCNN.PixelCNN_Context(7, 2), PixelCNN.PixelCNN_Context(4, 3),
             PixelCNN.PixelCNN_Context(1, 4)])
        self.coding_LH_list = torch.nn.ModuleList(
            [PixelCNN.PixelCNN_Context(11, 2), PixelCNN.PixelCNN_Context(8, 2), PixelCNN.PixelCNN_Context(5, 3),
             PixelCNN.PixelCNN_Context(2, 4)])
        self.coding_HH_list = torch.nn.ModuleList(
            [PixelCNN.PixelCNN_Context(12, 2), PixelCNN.PixelCNN_Context(9, 2), PixelCNN.PixelCNN_Context(6, 3),
             PixelCNN.PixelCNN_Context(3, 4)])
        # self.coding_HL_list = torch.nn.ModuleList([PixelCNN.PixelCNN_Context(10, 4),PixelCNN.PixelCNN_Context(7, 3),PixelCNN.PixelCNN_Context(4, 2),PixelCNN.PixelCNN_Context(1, 2)])
        # self.coding_LH_list = torch.nn.ModuleList([PixelCNN.PixelCNN_Context(11, 4),PixelCNN.PixelCNN_Context(8, 3),PixelCNN.PixelCNN_Context(5, 2),PixelCNN.PixelCNN_Context(2, 2)])
        # self.coding_HH_list = torch.nn.ModuleList([PixelCNN.PixelCNN_Context(12, 4),PixelCNN.PixelCNN_Context(9, 3),PixelCNN.PixelCNN_Context(6, 2),PixelCNN.PixelCNN_Context(3, 2)])

    def forward(self, x, enc):
        # forward transform
        LL = x
        HL_list = []
        LH_list = []
        HH_list = []
        for i in range(self.trans_steps):
            LL, HL, LH, HH = self.wavelet_transform[i].forward_trans(LL)
            HL_list.append(HL)
            LH_list.append(LH)
            HH_list.append(HH)

        bits = self.coding_LL(LL)
        context_list = [LL]

        for i in range(self.trans_steps):

            j = self.trans_steps - 1 - i

            bits = bits + self.coding_HL_list[j](HL_list[j], torch.cat(context_list, 1))
            context_list.append(HL_list[j])

            bits = bits + self.coding_LH_list[j](LH_list[j], torch.cat(context_list, 1))
            context_list.append(LH_list[j])

            bits = bits + self.coding_HH_list[j](HH_list[j], torch.cat(context_list, 1))
            context_list.append(HH_list[j])

            for k, context_feature in enumerate(context_list):
                context_list[k] = core.imresize(context_feature, scale=2)

            LL = self.wavelet_transform[j].inverse_trans(LL, HL_list[j], LH_list[j], HH_list[j])

        return bits, LL

