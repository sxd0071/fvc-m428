import torch
import torch.optim as optim
from torch.autograd import Variable
import math
import sys
from torch.nn import functional as F
import numpy as np


class Low_bound(torch.autograd.Function):
    @staticmethod
    def forward(ctx, x):
        ctx.save_for_backward(x)
        x = torch.clamp(x, min=1e-6)
        return x

    @staticmethod
    def backward(ctx, g):
        x, = ctx.saved_tensors
        grad1 = g.clone()
        grad1[x < 1e-6] = 0
        pass_through_if = torch.logical_or(x >= 1e-6, g < 0.0)
        t = pass_through_if + 0.0

        return grad1 * t


class Distribution_for_entropy2(torch.nn.Module):
    def __init__(self):
        super(Distribution_for_entropy2, self).__init__()

        self.relu = torch.nn.ReLU(inplace=False)

    def forward(self, x, p_dec):
        channel = p_dec.size()[1]
        if channel % 3 != 0:
            raise ValueError(
                "channel number must be multiple of 3")
        gauss_num = channel // 3
        temp = torch.chunk(p_dec, channel, dim=1)

        # keep the weight  summation of prob == 1
        probs = torch.cat(temp[gauss_num * 2:], dim=1)
        probs = F.softmax(probs, dim=1)

        scale = self.relu(p_dec[:, gauss_num:gauss_num * 2]) + 1e-6

        gauss_list = []
        for i in range(gauss_num):
            # gauss_list.append(torch.distributions.normal.Normal(temp[i], temp[i+gauss_num]))
            gauss_list.append(torch.distributions.normal.Normal(temp[i], scale[:, i:i + 1]))

        likelihood_list = []
        for i in range(gauss_num):
            likelihood_list.append(torch.abs(gauss_list[i].cdf(x + 0.5 / 255.) - gauss_list[i].cdf(x - 0.5 / 255.)))

        likelihoods = 0
        for i in range(gauss_num):
            likelihoods += probs[:, i:i + 1, :, :] * likelihood_list[i]

        return likelihoods


class MaskedConv2d(torch.nn.Conv2d):
    def __init__(self, mask_type, *args, **kwargs):
        super(MaskedConv2d, self).__init__(*args, **kwargs)
        assert mask_type in {'A', 'B'}
        self.register_buffer('mask', self.weight.data.clone())
        _, _, kH, kW = self.weight.size()
        self.mask.fill_(1)
        self.mask[:, :, kH // 2, kW // 2 + (mask_type == 'B'):] = 0
        self.mask[:, :, kH // 2 + 1:] = 0

    def forward(self, x):
        self.weight.data *= self.mask
        return super(MaskedConv2d, self).forward(x)


class MaskResBlock(torch.nn.Module):
    def __init__(self, internal_channel):
        super(MaskResBlock, self).__init__()

        self.conv1 = MaskedConv2d('B', in_channels=internal_channel, out_channels=internal_channel, kernel_size=3,
                                  stride=1, padding=0)
        self.conv2 = MaskedConv2d('B', in_channels=internal_channel, out_channels=internal_channel, kernel_size=3,
                                  stride=1, padding=0)
        self.relu = torch.nn.ReLU(inplace=False)

    def forward(self, x):
        out = self.conv1(x)
        out = self.relu(out)
        out = self.conv2(out)
        return out + x[:, :, 2:-2, 2:-2]


class ResBlock(torch.nn.Module):
    def __init__(self, internal_channel):
        super(ResBlock, self).__init__()
        self.conv1 = torch.nn.Conv2d(in_channels=internal_channel, out_channels=internal_channel, kernel_size=3,
                                     stride=1, padding=0)
        self.conv2 = torch.nn.Conv2d(in_channels=internal_channel, out_channels=internal_channel, kernel_size=3,
                                     stride=1, padding=0)
        self.relu = torch.nn.ReLU(inplace=False)

    def forward(self, x):
        out = self.conv1((x))
        out = self.relu(out)
        out = self.conv2((out))
        return out + x[:, :, 2:-2, 2:-2]


class PixelCNN(torch.nn.Module):
    def __init__(self):
        super(PixelCNN, self).__init__()

        self.internal_channel = 128
        self.num_params = 9

        self.relu = torch.nn.ReLU(inplace=False)

        self.padding_constant = torch.nn.ConstantPad2d(6, 0)

        self.conv_pre = MaskedConv2d('A', in_channels=1, out_channels=self.internal_channel, kernel_size=3, stride=1,
                                     padding=0)
        self.res1 = MaskResBlock(self.internal_channel)
        self.res2 = MaskResBlock(self.internal_channel)
        self.conv_post = MaskedConv2d('B', in_channels=self.internal_channel, out_channels=self.internal_channel,
                                      kernel_size=3, stride=1, padding=0)

        def infering():
            return torch.nn.Sequential(
                torch.nn.Conv2d(in_channels=self.internal_channel, out_channels=self.internal_channel, kernel_size=1,
                                stride=1, padding=0),
                torch.nn.ReLU(inplace=False),
                torch.nn.Conv2d(in_channels=self.internal_channel, out_channels=self.internal_channel, kernel_size=1,
                                stride=1, padding=0),
                torch.nn.ReLU(inplace=False),
                torch.nn.Conv2d(in_channels=self.internal_channel, out_channels=self.num_params, kernel_size=1,
                                stride=1, padding=0)
            )

        self.infer = infering()
        self.gaussin_entropy_func = Distribution_for_entropy2()

    def forward(self, x):
        x = x / 255.

        lable = x

        x = self.padding_constant(x)
        x = self.conv_pre(x)
        conv1 = x
        x = self.res1(x)
        x = self.res2(x)
        x = conv1[:, :, 4:-4, 4:-4] + x
        x = self.conv_post(x)
        x = self.relu(x)

        params = self.infer(x) + 1e-6

        prob = self.gaussin_entropy_func(lable, params)
        prob = Low_bound.apply(prob)

        bits = -torch.sum(torch.log2(prob))

        return bits


class PixelCNN_Context(torch.nn.Module):
    def __init__(self, context_num, check_step=2, shuffle_factor=2):
        super(PixelCNN_Context, self).__init__()

        self.internal_channel = 128
        self.num_params = 9
        self.check_step = check_step

        self.shuffle_factor = shuffle_factor
        self.pixel_shuffle = torch.nn.PixelShuffle(shuffle_factor)
        self.pixel_unshuffle = torch.nn.PixelUnshuffle(shuffle_factor)

        self.relu = torch.nn.ReLU(inplace=False)

        self.padding_constant_context = torch.nn.ConstantPad2d(6, 0)
        self.padding_constant = torch.nn.ConstantPad2d(6, 0)

        def infering():
            return torch.nn.Sequential(
                torch.nn.Conv2d(in_channels=self.internal_channel, out_channels=self.internal_channel, kernel_size=1,
                                stride=1, padding=0),
                torch.nn.ReLU(inplace=False),
                torch.nn.Conv2d(in_channels=self.internal_channel, out_channels=self.internal_channel, kernel_size=1,
                                stride=1, padding=0),
                torch.nn.ReLU(inplace=False),
                torch.nn.Conv2d(in_channels=self.internal_channel, out_channels=self.num_params * 2, kernel_size=1,
                                stride=1, padding=0)
            )

        self.conv_pre_c = torch.nn.ModuleList()
        self.res1_c = torch.nn.ModuleList()
        self.res2_c = torch.nn.ModuleList()
        self.conv_post = torch.nn.ModuleList()
        self.infer = torch.nn.ModuleList()
        for _ in range(check_step):
            self.res1_c.append(ResBlock(self.internal_channel))
            self.res2_c.append(ResBlock(self.internal_channel))
            self.conv_post.append(
                torch.nn.Conv2d(in_channels=self.internal_channel, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))
            self.infer.append(infering())
        if check_step == 2:
            self.conv_pre_c.append(
                torch.nn.Conv2d(in_channels=context_num * 4, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))
            self.conv_pre_c.append(
                torch.nn.Conv2d(in_channels=context_num * 4 + 2, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))
        elif check_step == 3:
            self.conv_pre_c.append(
                torch.nn.Conv2d(in_channels=context_num * 4, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))
            self.conv_pre_c.append(
                torch.nn.Conv2d(in_channels=context_num * 4 + 1, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))
            self.conv_pre_c.append(
                torch.nn.Conv2d(in_channels=context_num * 4 + 2, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))
        elif check_step == 4:
            self.conv_pre_c.append(
                torch.nn.Conv2d(in_channels=context_num * 4, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))
            self.conv_pre_c.append(
                torch.nn.Conv2d(in_channels=context_num * 4 + 1, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))
            self.conv_pre_c.append(
                torch.nn.Conv2d(in_channels=context_num * 4 + 2, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))
            self.conv_pre_c.append(
                torch.nn.Conv2d(in_channels=context_num * 4 + 3, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))

        elif check_step == 5:
            self.conv_pre_c.append(
                torch.nn.Conv2d(in_channels=context_num * 4, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))
            self.conv_pre_c.append(
                torch.nn.Conv2d(in_channels=context_num * 4 + 1, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))
            self.conv_pre_c.append(
                torch.nn.Conv2d(in_channels=context_num * 4 + 2, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))
            self.conv_pre_c.append(
                torch.nn.Conv2d(in_channels=context_num * 4 + 3, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))
            self.conv_pre_c.append(
                torch.nn.Conv2d(in_channels=context_num * 4 + 4, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))

        self.gaussin_entropy_func = Distribution_for_entropy2()

        self.context_num = context_num

    def forward(self, x, context):

        x = x / 255.
        context = context / 255.

        unshuffled_x = self.pixel_unshuffle(x)
        unshuffled_x = [unshuffled_x[:, 0:1], unshuffled_x[:, 1:2], unshuffled_x[:, 2:3], unshuffled_x[:, 3:4]]
        unshuffled_context = self.pixel_unshuffle(context)
        # lable = torch.cat([torch.cat([unshuffled_x[0], unshuffled_x[-1]], dim=2), torch.cat([unshuffled_x[1], unshuffled_x[2]], dim=2)], dim=3)

        bits = 0

        if self.check_step == 2:

            for i in range(self.check_step):
                context_tmp = self.padding_constant_context(unshuffled_context)
                context_tmp = self.conv_pre_c[i]((context_tmp))
                context_tmp = self.res1_c[i](context_tmp)
                context_tmp = self.res2_c[i](context_tmp)
                context_tmp = self.relu(context_tmp)
                context_tmp = self.conv_post[i](context_tmp)
                param_tmp = self.infer[i](context_tmp)
                param_tmp_1, param_tmp_2 = param_tmp.chunk(2, 1)

                if i == 0:
                    prob_1 = self.gaussin_entropy_func(unshuffled_x[0], param_tmp_1)
                    prob_2 = self.gaussin_entropy_func(unshuffled_x[3], param_tmp_2)
                elif i == 1:
                    prob_1 = self.gaussin_entropy_func(unshuffled_x[1], param_tmp_1)
                    prob_2 = self.gaussin_entropy_func(unshuffled_x[2], param_tmp_2)
                else:
                    print('not support')
                    exit()

                prob_1 = Low_bound.apply(prob_1)
                prob_2 = Low_bound.apply(prob_2)

                bits += -torch.sum(torch.log2(prob_1))
                bits += -torch.sum(torch.log2(prob_2))

                unshuffled_context = torch.cat([unshuffled_context, unshuffled_x[0], unshuffled_x[3]], dim=1)

        elif self.check_step == 3:

            for i in range(self.check_step):
                context_tmp = self.padding_constant_context(unshuffled_context)
                context_tmp = self.conv_pre_c[i]((context_tmp))
                context_tmp = self.res1_c[i](context_tmp)
                context_tmp = self.res2_c[i](context_tmp)
                context_tmp = self.relu(context_tmp)
                context_tmp = self.conv_post[i](context_tmp)
                param_tmp = self.infer[i](context_tmp)

                if i == 0:
                    prob_1 = self.gaussin_entropy_func(unshuffled_x[0], param_tmp)
                    prob_1 = Low_bound.apply(prob_1)
                    bits += -torch.sum(torch.log2(prob_1))
                    unshuffled_context = torch.cat([unshuffled_context, unshuffled_x[0]], dim=1)
                elif i == 1:
                    prob_2 = self.gaussin_entropy_func(unshuffled_x[1], param_tmp)
                    prob_2 = Low_bound.apply(prob_2)
                    bits += -torch.sum(torch.log2(prob_2))
                    unshuffled_context = torch.cat([unshuffled_context, unshuffled_x[1]], dim=1)
                elif i == 2:
                    param_tmp_1, param_tmp_2 = param_tmp.chunk(2, 1)
                    prob_1 = self.gaussin_entropy_func(unshuffled_x[2], param_tmp_1)
                    prob_2 = self.gaussin_entropy_func(unshuffled_x[3], param_tmp_2)
                    prob_1 = Low_bound.apply(prob_1)
                    prob_2 = Low_bound.apply(prob_2)
                    bits += -torch.sum(torch.log2(prob_1))
                    bits += -torch.sum(torch.log2(prob_2))
                else:
                    print('not support')
                    exit()

        elif self.check_step == 4:

            for i in range(self.check_step):
                context_tmp = self.padding_constant_context(unshuffled_context)
                context_tmp = self.conv_pre_c[i]((context_tmp))
                context_tmp = self.res1_c[i](context_tmp)
                context_tmp = self.res2_c[i](context_tmp)
                context_tmp = self.relu(context_tmp)
                context_tmp = self.conv_post[i](context_tmp)
                param_tmp = self.infer[i](context_tmp)

                if i == 0:
                    prob = self.gaussin_entropy_func(unshuffled_x[0], param_tmp)
                    prob = Low_bound.apply(prob)
                    bits += -torch.sum(torch.log2(prob))
                    unshuffled_context = torch.cat([unshuffled_context, unshuffled_x[0]], dim=1)
                elif i == 1:
                    prob = self.gaussin_entropy_func(unshuffled_x[3], param_tmp)
                    prob = Low_bound.apply(prob)
                    bits += -torch.sum(torch.log2(prob))
                    unshuffled_context = torch.cat([unshuffled_context, unshuffled_x[3]], dim=1)
                elif i == 2:
                    prob = self.gaussin_entropy_func(unshuffled_x[1], param_tmp)
                    prob = Low_bound.apply(prob)
                    bits += -torch.sum(torch.log2(prob))
                    unshuffled_context = torch.cat([unshuffled_context, unshuffled_x[1]], dim=1)
                elif i == 3:
                    prob = self.gaussin_entropy_func(unshuffled_x[2], param_tmp)
                    prob = Low_bound.apply(prob)
                    bits += -torch.sum(torch.log2(prob))
                else:
                    print('not support')
                    exit()

        elif self.check_step == 5:
            for i in range(self.check_step):
                context_tmp = self.padding_constant_context(unshuffled_context)
                context_tmp = self.conv_pre_c[i]((context_tmp))
                context_tmp = self.res1_c[i](context_tmp)
                context_tmp = self.res2_c[i](context_tmp)
                context_tmp = self.relu(context_tmp)
                context_tmp = self.conv_post[i](context_tmp)
                param_tmp = self.infer[i](context_tmp)

                if i == 0:
                    prob = self.gaussin_entropy_func(unshuffled_x[0], param_tmp)
                    prob = Low_bound.apply(prob)
                    bits += -torch.sum(torch.log2(prob))
                    unshuffled_context = torch.cat([unshuffled_context, unshuffled_x[0]], dim=1)
                elif i == 1:
                    prob = self.gaussin_entropy_func(unshuffled_x[3], param_tmp)
                    prob = Low_bound.apply(prob)
                    bits += -torch.sum(torch.log2(prob))
                    unshuffled_context = torch.cat([unshuffled_context, unshuffled_x[3]], dim=1)
                elif i == 2:
                    prob = self.gaussin_entropy_func(unshuffled_x[1], param_tmp)
                    prob = Low_bound.apply(prob)
                    bits += -torch.sum(torch.log2(prob))
                    unshuffled_context = torch.cat([unshuffled_context, unshuffled_x[1]], dim=1)
                elif i == 3:
                    prob = self.gaussin_entropy_func(unshuffled_x[2], param_tmp)
                    prob = Low_bound.apply(prob)
                    bits += -torch.sum(torch.log2(prob))
                    unshuffled_context = torch.cat([unshuffled_context, unshuffled_x[2]], dim=1)

                elif i == 4:
                    prob = self.gaussin_entropy_func(unshuffled_x[0], param_tmp)
                    prob = Low_bound.apply(prob)
                    bits += -torch.sum(torch.log2(prob))
                else:
                    print('not support')
                    exit()
        return bits


