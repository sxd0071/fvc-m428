import logging
import sys
import argparse
import glob, os
import time
import pathlib
from os.path import abspath, join, dirname, pardir

sys.path.append(join(abspath(dirname(__file__)), pardir))
import torch
from Common.utils.bee_utils.testfuncs import print_args, get_device, get_model_list
from Common.model_engine import ModelEngine

torch.backends.cudnn.deterministic = True


def parse_args(argv):
    parser = argparse.ArgumentParser(description="Decoder arguments parser.")
    parser.add_argument('-i', '--input', type=str, help='Input image file path.', default=None)
    parser.add_argument('--inputPath', type=str, help='Input image file path.', default=None)
    parser.add_argument('-o', '--output', type=str, default=None, help='Output bin file name')
    parser.add_argument('--outputPath', type=str, default='bitstreams', help='Output bin file path')
    parser.add_argument('--device', type=str, choices=['cpu', 'cuda'], default='cuda', help='CPU or GPU device.')
    parser.add_argument("--ckptdir", type=str, help='Checkpoint folder containing multiple pretrained models.')
    parser.add_argument("--cfg", type=str, help='Path to the CfG file', default="./Encoder/cfg/bee_cfg/cfg.json")
    parser.add_argument("--target_rate", type=float, nargs='+', default=[0.75],
                        help="Target bpp (default: %(default)s)")

    # NIC Arguments
    parser.add_argument("-m", "--model", type=int, default=None, help="Model Index")
    parser.add_argument("--lambda_rd", type=float, default=1, help="Input lambda for variable-rate models")
    parser.add_argument('--outlog', type=str, default='./test_time/EncTime.log')

    args = parser.parse_args(argv)

    # Check for validity; inputPath and output should't be used together
    if args.inputPath is not None and args.output is not None:
        raise ValueError("'inputPath' and 'output' cannot be used together. Use 'inputPath' with 'outputPath'.")

    return args


def encode(args):
    device = get_device(args.device)
    if args.input == None and args.inputPath == None:
        print("either the input image name or the folder path needs to be provided.")
        return []
    if args.ckptdir == None:
        print("either the ckptdir needs to be provided.")
        return []
    if args.inputPath:
        images = glob.glob(os.path.join(args.inputPath, "*.png"))
        if images == []:
            print("No files found found in the images directory: ", args.inputPath)
            return []
        images.sort()
    else:
        images = [args.input]
    if not os.path.exists(args.outputPath):
        os.makedirs(args.outputPath)
    for s in images:
        # NIC
        if args.model != None:
            args.target_rate = [[args.model, args.lambda_rd]]
        
        log_dir = os.path.dirname(args.outlog)
        os.makedirs(log_dir, exist_ok=True)
        logging.basicConfig(filename=args.outlog, level=logging.INFO)
        for rate in args.target_rate:
            imagename = s.split("/")[-1]
            print(imagename, rate)

            # Generate name for bit file
            if args.output is not None:
                binName = args.output
            else:
                binPath = args.outputPath
                binName = pathlib.Path(imagename).stem+".bin"
                binName = os.path.join(binPath, binName)

            enc_engine = ModelEngine()
            start_time = time.time()
            enc_engine.encode(args.cfg, s, rate, args.ckptdir, binName, device)
            enc_time = time.time() - start_time
            logging.info(f'{s} {enc_time}')
            torch.cuda.empty_cache()

def main(argv):
    args = parse_args(argv[1:])
    print_args(args)
    torch.set_num_threads(1)  # just to be sure
    encode(args)

if __name__ == '__main__':
    main(sys.argv)
