import torch
from torch.nn import functional as F


class Low_bound(torch.autograd.Function):
    @staticmethod
    def forward(ctx, x):
        ctx.save_for_backward(x)
        x = torch.clamp(x, min=1e-6)
        return x

    @staticmethod
    def backward(ctx, g):
        x, = ctx.saved_tensors
        grad1 = g.clone()
        grad1[x < 1e-6] = 0
        pass_through_if = torch.logical_or(x >= 1e-6, g < 0.0)
        t = pass_through_if + 0.0

        return grad1 * t


class Distribution_for_entropy2(torch.nn.Module):
    def __init__(self):
        super(Distribution_for_entropy2, self).__init__()

        self.relu = torch.nn.ReLU(inplace=False)

    def forward(self, x, p_dec):
        channel = p_dec.size()[1]
        if channel % 3 != 0:
            raise ValueError(
                "channel number must be multiple of 3")
        gauss_num = channel // 3
        temp = torch.chunk(p_dec, channel, dim=1)

        probs = torch.cat(temp[gauss_num * 2:], dim=1)
        probs = F.softmax(probs, dim=1)

        scale = self.relu(p_dec[:, gauss_num:gauss_num * 2]) + 1e-6
        del p_dec

        gauss_list = []
        for i in range(gauss_num):
            gauss_list.append(torch.distributions.normal.Normal(temp[i], scale[:, i:i + 1]))
        del temp

        likelihood_list = []
        for i in range(gauss_num):
            likelihood_list.append(torch.abs(gauss_list[i].cdf(x + 0.5 / 255.) - gauss_list[i].cdf(x - 0.5 / 255.)))

        likelihoods = 0
        for i in range(gauss_num):
            likelihoods += probs[:, i:i + 1, :, :] * likelihood_list[i]

        return likelihoods

    def cdf_lower(self, x, p_dec):
        channel = p_dec.size()[1]
        if channel % 3 != 0:
            raise ValueError(
                "channel number must be multiple of 3")
        gauss_num = channel // 3
        temp = torch.chunk(p_dec, channel, dim=1)

        # keep the weight  summation of prob == 1
        probs = torch.cat(temp[gauss_num * 2:], dim=1)
        probs = F.softmax(probs, dim=1)

        scale = self.relu(p_dec[:, gauss_num:gauss_num * 2]) + 1e-6
        del p_dec

        gauss_list = []
        for i in range(gauss_num):
            gauss_list.append(torch.distributions.normal.Normal(temp[i], scale[:, i:i + 1]))
        del temp

        likelihood_list = []
        for i in range(gauss_num):
            likelihood_list.append(gauss_list[i].cdf(x - 0.5 / 255.))

        likelihoods = 0
        for i in range(gauss_num):
            likelihoods += probs[:, i:i + 1, :, :] * likelihood_list[i]

        return likelihoods


class MaskedConv2d(torch.nn.Conv2d):
    def __init__(self, mask_type, *args, **kwargs):
        super(MaskedConv2d, self).__init__(*args, **kwargs)
        assert mask_type in {'A', 'B'}
        self.register_buffer('mask', self.weight.data.clone())
        _, _, kH, kW = self.weight.size()
        self.mask.fill_(1)
        self.mask[:, :, kH // 2, kW // 2 + (mask_type == 'B'):] = 0
        self.mask[:, :, kH // 2 + 1:] = 0

    def forward(self, x):
        self.weight.data *= self.mask
        return super(MaskedConv2d, self).forward(x)


class MaskResBlock(torch.nn.Module):
    def __init__(self, internal_channel):
        super(MaskResBlock, self).__init__()

        self.conv1 = MaskedConv2d('B', in_channels=internal_channel, out_channels=internal_channel, kernel_size=3,
                                  stride=1, padding=0)
        self.conv2 = MaskedConv2d('B', in_channels=internal_channel, out_channels=internal_channel, kernel_size=3,
                                  stride=1, padding=0)
        self.relu = torch.nn.ReLU(inplace=False)

    def forward(self, x):
        out = self.conv1(x)
        out = self.relu(out)
        out = self.conv2(out)
        return out + x[:, :, 2:-2, 2:-2]


class ResBlock(torch.nn.Module):
    def __init__(self, internal_channel):
        super(ResBlock, self).__init__()
        self.conv1 = torch.nn.Conv2d(in_channels=internal_channel, out_channels=internal_channel, kernel_size=3,
                                     stride=1, padding=0)
        self.conv2 = torch.nn.Conv2d(in_channels=internal_channel, out_channels=internal_channel, kernel_size=3,
                                     stride=1, padding=0)
        self.relu = torch.nn.ReLU(inplace=False)

    def forward(self, x):
        out = self.conv1((x))
        out = self.relu(out)
        out = self.conv2((out))
        return out + x[:, :, 2:-2, 2:-2]


class PixelCNN(torch.nn.Module):
    def __init__(self):
        super(PixelCNN, self).__init__()

        self.internal_channel = 128
        self.num_params = 9

        self.relu = torch.nn.ReLU(inplace=False)

        self.padding_constant = torch.nn.ConstantPad2d(6, 0)

        self.conv_pre = MaskedConv2d('A', in_channels=1, out_channels=self.internal_channel, kernel_size=3, stride=1,
                                     padding=0)
        self.res1 = MaskResBlock(self.internal_channel)
        self.res2 = MaskResBlock(self.internal_channel)
        self.conv_post = MaskedConv2d('B', in_channels=self.internal_channel, out_channels=self.internal_channel,
                                      kernel_size=3, stride=1, padding=0)

        def infering():
            return torch.nn.Sequential(
                torch.nn.Conv2d(in_channels=self.internal_channel, out_channels=self.internal_channel, kernel_size=1,
                                stride=1, padding=0),
                torch.nn.ReLU(inplace=False),
                torch.nn.Conv2d(in_channels=self.internal_channel, out_channels=self.internal_channel, kernel_size=1,
                                stride=1, padding=0),
                torch.nn.ReLU(inplace=False),
                torch.nn.Conv2d(in_channels=self.internal_channel, out_channels=self.num_params, kernel_size=1,
                                stride=1, padding=0)
            )

        self.infer = infering()
        self.gaussin_entropy_func = Distribution_for_entropy2()

    def forward(self, x, range_low_bond, range_high_bond):
        label = torch.arange(start=range_low_bond, end=range_high_bond + 1, dtype=torch.float,
                             device=torch.device('cuda'))
        label = label.unsqueeze(0).unsqueeze(0).unsqueeze(0).unsqueeze(0) / 255.
        x = x / 255.

        x = self.conv_pre(x)
        conv1 = x
        x = self.res1(x)
        x = self.res2(x)
        x = conv1[:, :, 4:-4, 4:-4] + x
        x = self.conv_post(x)
        x = self.relu(x)

        params = self.infer(x) + 1e-6
        params = params.unsqueeze(-1).repeat(1, 1, 1, 1, range_high_bond - range_low_bond + 1)

        prob = self.gaussin_entropy_func(label, params)

        prob = prob + 1e-5

        return prob.squeeze(1)
        # prob [N, H, W, range]

    def inf_cdf_lower(self, x, range_low_bond, range_high_bond):
        label = torch.arange(start=range_low_bond, end=range_high_bond + 1, dtype=torch.float,
                             device=torch.device('cuda'))
        label = label.unsqueeze(0).unsqueeze(0).unsqueeze(0).unsqueeze(0) / 255.
        x = x / 255.

        x = self.conv_pre(x)
        conv1 = x
        x = self.res1(x)
        x = self.res2(x)
        x = conv1[:, :, 4:-4, 4:-4] + x
        x = self.conv_post(x)
        x = self.relu(x)

        params = self.infer(x).unsqueeze(-1) + 1e-6
        params = params.repeat(1, 1, 1, 1, range_high_bond - range_low_bond + 1)

        prob = self.gaussin_entropy_func.cdf_lower(label, params)

        return prob.squeeze(1).squeeze(1).squeeze(1)


class PixelCNN_Context(torch.nn.Module):
    def __init__(self, context_num, check_step):
        super(PixelCNN_Context, self).__init__()

        self.internal_channel = 128
        self.num_params = 9

        self.relu = torch.nn.ReLU(inplace=False)

        self.padding_constant_context = torch.nn.ConstantPad2d(6, 0)
        self.padding_constant = torch.nn.ConstantPad2d(6, 0)

        def infering():
            return torch.nn.Sequential(
                torch.nn.Conv2d(in_channels=self.internal_channel, out_channels=self.internal_channel, kernel_size=1,
                                stride=1, padding=0),
                torch.nn.ReLU(inplace=False),
                torch.nn.Conv2d(in_channels=self.internal_channel, out_channels=self.internal_channel, kernel_size=1,
                                stride=1, padding=0),
                torch.nn.ReLU(inplace=False),
                torch.nn.Conv2d(in_channels=self.internal_channel, out_channels=self.num_params * 2, kernel_size=1,
                                stride=1, padding=0)
            )

        self.conv_pre_c = torch.nn.ModuleList()
        self.res1_c = torch.nn.ModuleList()
        self.res2_c = torch.nn.ModuleList()
        self.conv_post = torch.nn.ModuleList()
        self.infer = torch.nn.ModuleList()
        for _ in range(check_step):
            self.res1_c.append(ResBlock(self.internal_channel))
            self.res2_c.append(ResBlock(self.internal_channel))
            self.conv_post.append(
                torch.nn.Conv2d(in_channels=self.internal_channel, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))
            self.infer.append(infering())
        if check_step == 2:
            self.conv_pre_c.append(
                torch.nn.Conv2d(in_channels=context_num * 4, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))
            self.conv_pre_c.append(
                torch.nn.Conv2d(in_channels=context_num * 4 + 2, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))
        elif check_step == 3:
            self.conv_pre_c.append(
                torch.nn.Conv2d(in_channels=context_num * 4, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))
            self.conv_pre_c.append(
                torch.nn.Conv2d(in_channels=context_num * 4 + 1, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))
            self.conv_pre_c.append(
                torch.nn.Conv2d(in_channels=context_num * 4 + 2, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))
        elif check_step == 4:
            self.conv_pre_c.append(
                torch.nn.Conv2d(in_channels=context_num * 4, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))
            self.conv_pre_c.append(
                torch.nn.Conv2d(in_channels=context_num * 4 + 1, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))
            self.conv_pre_c.append(
                torch.nn.Conv2d(in_channels=context_num * 4 + 2, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))
            self.conv_pre_c.append(
                torch.nn.Conv2d(in_channels=context_num * 4 + 3, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))

        elif check_step == 5:
            self.conv_pre_c.append(
                torch.nn.Conv2d(in_channels=context_num * 4, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))
            self.conv_pre_c.append(
                torch.nn.Conv2d(in_channels=context_num * 4 + 1, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))
            self.conv_pre_c.append(
                torch.nn.Conv2d(in_channels=context_num * 4 + 2, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))
            self.conv_pre_c.append(
                torch.nn.Conv2d(in_channels=context_num * 4 + 3, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))
            self.conv_pre_c.append(
                torch.nn.Conv2d(in_channels=context_num * 4 + 4, out_channels=self.internal_channel, kernel_size=3,
                                stride=1, padding=0))

        self.gaussin_entropy_func = Distribution_for_entropy2()

        self.context_num = context_num

    def forward(self, context, range_low_bond, range_high_bond, idx):

        label = torch.arange(start=range_low_bond, end=range_high_bond + 1, dtype=torch.float,
                             device=torch.device('cuda'))
        label = label.unsqueeze(0).unsqueeze(0).unsqueeze(0).unsqueeze(0) / 255.

        context = context / 255.
        context = self.conv_pre_c[idx]((context))

        context = self.res1_c[idx](context)
        context = self.res2_c[idx](context)

        context = self.relu(context)
        context = self.conv_post[idx](context)

        params = self.infer[idx](context)

        params = params.unsqueeze(-1).repeat(1, 1, 1, 1, range_high_bond - range_low_bond + 1)

        prob = self.gaussin_entropy_func(label, params)
        prob = prob + 1e-5

        return prob

    def inf_cdf_lower(self, context, range_low_bond, range_high_bond, idx, split=0):

        label = torch.arange(start=range_low_bond, end=range_high_bond + 1, dtype=torch.float,
                             device=torch.device('cuda'))
        label = label.unsqueeze(0).unsqueeze(0).unsqueeze(0).unsqueeze(0) / 255.

        context = context / 255.

        context = self.conv_pre_c[idx]((context))

        context = self.res1_c[idx](context)
        context = self.res2_c[idx](context)

        context = self.relu(context)
        context = self.conv_post[idx](context)

        params = self.infer[idx](context)

        if (split == 0):
            params = params.unsqueeze(-1).repeat(1, 1, 1, 1, range_high_bond - range_low_bond + 1)

            prob = self.gaussin_entropy_func.cdf_lower(label, params)
        else:
            param_tmp_1, param_tmp_2 = params.chunk(2, 1)
            param_tmp_1 = param_tmp_1.unsqueeze(-1).repeat(1, 1, 1, 1, range_high_bond - range_low_bond + 1)
            prob1 = self.gaussin_entropy_func.cdf_lower(label, param_tmp_1)
            param_tmp_2 = param_tmp_2.unsqueeze(-1).repeat(1, 1, 1, 1, range_high_bond - range_low_bond + 1)
            prob2 = self.gaussin_entropy_func.cdf_lower(label, param_tmp_2)
            prob = [prob1, prob2]
        return prob

