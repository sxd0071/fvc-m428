import torch
import torch.nn as nn
import torch.nn.functional as F
import einops

from collections import OrderedDict

def TorchRound():
    """
    Apply STE to clamp function.
    """
    class identity_quant(torch.autograd.Function):
        @staticmethod
        def forward(ctx, input):
            out = torch.round(input)
            return out

        @staticmethod
        def backward(ctx, grad_output):
            return grad_output

    return identity_quant().apply

class SpaceToDepth(nn.Module):
    def __init__(self, block_size=4):
        super().__init__()
        assert block_size in {2, 4}, "Space2Depth only supports blocks size = 4 or 2"
        self.block_size = block_size

    def forward(self, x):
        N, C, H, W = x.size()
        S = self.block_size
        x = x.view(N, C, H // S, S, W // S, S)  # (N, C, H//bs, bs, W//bs, bs)
        x = x.permute(0, 3, 5, 1, 2, 4).contiguous()  # (N, bs, bs, C, H//bs, W//bs)
        x = x.view(N, C * S * S, H // S, W // S)  # (N, C*bs^2, H//bs, W//bs)
        return x

class DepthToSpace(nn.Module):
    def __init__(self, block_size=4):
        super().__init__()
        assert block_size in {2, 4}, "DepthToSpace only supports blocks size = 4 or 2"
        self.block_size = block_size

    def forward(self, x):
        N, C, H, W = x.size()
        S = self.block_size
        x = x.view(N,  S, S, C//S//S, H, W)  # (N, C, H//bs, bs, W//bs, bs)
        x = x.permute(0, 3, 4, 1, 5, 2).contiguous()  # (N, bs, bs, C, H//bs, W//bs)
        x = x.view(N, C//S//S, H * S, W * S)  # (N, C*bs^2, H//bs, W//bs)
        return x

class MultiFieldFusion(nn.Module):
    def __init__(self, ch_in):
        super(MultiFieldFusion, self).__init__()

        self.conv_0 = nn.Conv2d(ch_in, ch_in, kernel_size=3, stride=1, padding=1)
        self.conv_1 = nn.Conv2d(ch_in, ch_in, kernel_size=5, stride=1, padding=2)

        self.init_weight()

    def init_weight(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.xavier_normal_(m.weight)
                nn.init.constant_(m.bias, 0.01)

        # print("set xavier_uniform for nn.Conv2d.")

    def forward(self, x):
        x1 = self.conv_0(x)
        x2 = self.conv_1(x)
        x = torch.cat([x1, x2], 1)

        return x

class ResLite(nn.Module):
    def __init__(self, cin, scale=4):
        super(ResLite, self).__init__()    

        self.bypass131 = nn.Sequential(OrderedDict([
                ('conv_0', nn.Conv2d(cin, cin, kernel_size=3, stride=1, padding=1)),
                ('conv_1', nn.Conv2d(cin, cin*scale, kernel_size=1, stride=1, padding=0)),
                ('LR0', nn.LeakyReLU(negative_slope=0.2)),
                ('conv_2', nn.Conv2d(cin*scale, cin, kernel_size=1, stride=1, padding=0)),
                ]))

    def forward(self, x):

        output = x + self.bypass131(x)

        return output
    
class SSC(nn.Module):
    def __init__(self, ch_in):
        super(SSC, self).__init__()
        self.ch_in = ch_in
          
        self.space_to_depth = SpaceToDepth(2)
        self.depth_to_space = DepthToSpace(2)

        self.entropy_para1 = nn.Sequential(OrderedDict([
            ("conv_0", nn.Conv2d(ch_in*2, ch_in*2, kernel_size=3, stride=1, padding=1)),
            ('LR0', nn.LeakyReLU(negative_slope=0.2)),
            ("conv_1", nn.Conv2d(ch_in*2, ch_in, kernel_size=3, stride=1, padding=1)),
            ("ResLite0", ResLite(ch_in)),
            ("ResLite1", ResLite(ch_in)),
            ("ResLite2", ResLite(ch_in)),
            # ("conv_up", nn.Conv2d(ch_in, ch_in, kernel_size=4, stride=2, padding=1)),
            ]))
        
        self.entropy_para2 = nn.Sequential(OrderedDict([
            ("conv_0", nn.Conv2d(ch_in*2+ch_in, ch_in*2, kernel_size=3, stride=1, padding=1)),
            ('LR0', nn.LeakyReLU(negative_slope=0.2)),
            ("conv_1", nn.Conv2d(ch_in*2, ch_in, kernel_size=3, stride=1, padding=1)),
            ("ResLite0", ResLite(ch_in)),
            ("ResLite1", ResLite(ch_in)),
            ("ResLite2", ResLite(ch_in)),
            # ("conv_up", nn.Conv2d(ch_in, ch_in*2, kernel_size=4, stride=2, padding=1)),
            ]))
        
        self.field_fusion_conv = MultiFieldFusion(ch_in//2)
        self.identity_round = TorchRound()

        self.init_weight()
        
    def init_weight(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.xavier_normal_(m.weight)
                nn.init.constant_(m.bias, 0.01)
                
        # print("set xavier_uniform for nn.Conv2d.")

    def forward(self, y, aux_context):
        
        p1_mu = self.entropy_para1(aux_context)#H W Cin

        p1_y = self.identity_round(y - p1_mu) + p1_mu
        
        p1_y = self.space_to_depth(p1_y)[:, self.ch_in:self.ch_in*3]#H/2 W/2 Cin*2
        p1_y = self.depth_to_space(p1_y) #H W Cin/2
        p1_context = self.field_fusion_conv(p1_y)

        p2_mu = self.entropy_para2(torch.cat([aux_context, p1_context], 1))#H W Cin
        
        _, mu2, mu3, _ = torch.chunk(self.space_to_depth(p1_mu), 4, dim=1)
        mu1, _, _, mu4 = torch.chunk(self.space_to_depth(p2_mu), 4, dim=1)

        mu = self.depth_to_space(torch.cat([mu1, mu2, mu3, mu4], 1))
        
        y_hat = self.identity_round(y - mu) + mu
    
        return y_hat, mu

    def get_mask(self, scales_hat, filtercoeffs):
        mode = filtercoeffs["mode"]
        thr = filtercoeffs["thr"]
        block_size = filtercoeffs["block_size"]
        greater = filtercoeffs["greater"]
        scale = filtercoeffs["scale"]
        channels = filtercoeffs["channels"]
        mask = None
        self.likely = torch.abs(scales_hat)

        _, _, h, w = scales_hat.shape
        h_pad = ((h + block_size - 1) // block_size) * block_size - h
        w_pad = ((w + block_size - 1) // block_size) * block_size - w
        likely = F.pad(self.likely, (0, w_pad, 0, h_pad), value=1)
        if mode == 1:  # minpool
            maxpool = torch.nn.MaxPool2d((block_size, block_size), (block_size, block_size), 0)
            likely = -(maxpool(-likely))
        if mode == 2:  # avgpool
            avgpool = torch.nn.AvgPool2d((block_size, block_size), (block_size, block_size), 0)
            likely = avgpool(likely)
        if mode == 3:  # maxpool
            maxpool = torch.nn.MaxPool2d((block_size, block_size), (block_size, block_size), 0)
            likely = (maxpool(likely))

        if greater:
            mask = (likely > thr)
        else:
            mask = (likely < thr)
        if mode == 4:  # maxpool
            for i in range(192):
                if i in channels:
                    pass
                else:
                    mask[:, i, :, :] = False
        mask = einops.repeat(mask, 'a b c d -> a b (c repeat1) (d repeat2)', repeat1=block_size, repeat2=block_size)
        mask = F.pad(mask, (0, -w_pad, 0, -h_pad), value=1)
        return mask, scale

    def encode(self, y, aux_context, scale_hat, filterCoeffs):
        
        p1_mu = self.entropy_para1(aux_context)#H W Cin
        mask1 = []
        scale1 = []
        for i in range(3):
            mask, scale = self.get_mask(scale_hat, dict(filterCoeffs[i]))
            mask1.append(mask)
            scale1.append(torch.tensor(scale[0]))

        p1_y = y - p1_mu
        for i in range(3):
            p1_y = torch.where(mask1[i], p1_y * scale1[i], p1_y)

        p1_y = self.identity_round(p1_y)

        for i in range(3 - 1, -1, -1):
            p1_y = torch.where(mask1[i], p1_y / scale1[i], p1_y)

        p1_y = p1_y + p1_mu
        
        p1_y = self.space_to_depth(p1_y)[:, self.ch_in:self.ch_in*3]#H/2 W/2 Cin*2
        p1_y = self.depth_to_space(p1_y) #H W Cin/2
        p1_context = self.field_fusion_conv(p1_y)

        p2_mu = self.entropy_para2(torch.cat([aux_context, p1_context], 1))#H W Cin
        
        _, mu2, mu3, _ = torch.chunk(self.space_to_depth(p1_mu), 4, dim=1)
        mu1, _, _, mu4 = torch.chunk(self.space_to_depth(p2_mu), 4, dim=1)

        mu = self.depth_to_space(torch.cat([mu1, mu2, mu3, mu4], 1))

        w_hat = y - mu
        for i in range(3):
            w_hat = torch.where(mask1[i], w_hat * scale1[i], w_hat)

        w_hat = y_hat = self.identity_round(w_hat)

        for i in range(3 - 1, -1, -1):
            y_hat = torch.where(mask1[i], y_hat / scale1[i], y_hat)

        y_hat = y_hat + mu
    
        return y_hat, w_hat, mu

    def decode(self, w_hat, aux_context):
        
        p1_mu = self.entropy_para1(aux_context)#H W Cin
        
        p1_y = self.space_to_depth(w_hat+p1_mu)[:, self.ch_in:self.ch_in*3]#H/2 W/2 Cin*2
        p1_y = self.depth_to_space(p1_y) #H W Cin/2
        
        p1_context = self.field_fusion_conv(p1_y)

        p2_mu = self.entropy_para2(torch.cat([aux_context, p1_context], 1))#H W Cin
        
        _, mu2, mu3, _ = torch.chunk(self.space_to_depth(p1_mu), 4, dim=1)
        mu1, _, _, mu4 = torch.chunk(self.space_to_depth(p2_mu), 4, dim=1)

        mu = self.depth_to_space(torch.cat([mu1, mu2, mu3, mu4], 1))
        
        y_hat = w_hat + mu
    
        return y_hat, mu

class CSSC(nn.Module):
    def __init__(self, cin):
        super(CSSC, self).__init__()

        self.cin = cin
        self.slice = cin // 4
            
        self.SSC_0 = SSC(self.slice)
        self.SSC_1 = SSC(self.slice)
        self.SSC_2 = SSC(self.slice)
        self.SSC_3 = SSC(self.slice)

        self.aux_info_net0 = nn.Sequential(OrderedDict([
            ("conv_0", nn.Conv2d(self.cin*2, self.slice*4, kernel_size=1, stride=1, padding=0)),
            ('LR0', nn.LeakyReLU(negative_slope=0.2)),
            ("conv_1", nn.Conv2d(self.slice*4, self.slice*2, kernel_size=3, stride=1, padding=1)),
            ('LR0', nn.LeakyReLU(negative_slope=0.2)),
            ("conv_2", nn.Conv2d(self.slice*2, self.slice*2, kernel_size=3, stride=1, padding=1)),
            ]))
        
        self.aux_info_net1 = nn.Sequential(OrderedDict([
            ("conv_0", nn.Conv2d(self.cin*2+self.slice, self.slice*4, kernel_size=1, stride=1, padding=0)),
            ('LR0', nn.LeakyReLU(negative_slope=0.2)),
            ("conv_1", nn.Conv2d(self.slice*4, self.slice*2, kernel_size=3, stride=1, padding=1)),
            ('LR0', nn.LeakyReLU(negative_slope=0.2)),
            ("conv_2", nn.Conv2d(self.slice*2, self.slice*2, kernel_size=3, stride=1, padding=1)),
            ]))
        
        self.aux_info_net2 = nn.Sequential(OrderedDict([
            ("conv_0", nn.Conv2d(self.cin*2+self.slice*2, self.slice*4, kernel_size=1, stride=1, padding=0)),
            ('LR0', nn.LeakyReLU(negative_slope=0.2)),
            ("conv_1", nn.Conv2d(self.slice*4, self.slice*2, kernel_size=3, stride=1, padding=1)),
            ('LR0', nn.LeakyReLU(negative_slope=0.2)),
            ("conv_2", nn.Conv2d(self.slice*2, self.slice*2, kernel_size=3, stride=1, padding=1)),
            ]))

        self.aux_info_net3 = nn.Sequential(OrderedDict([
            ("conv_0", nn.Conv2d(self.cin*2+self.slice*3, self.slice*4, kernel_size=1, stride=1, padding=0)),
            ('LR0', nn.LeakyReLU(negative_slope=0.2)),
            ("conv_1", nn.Conv2d(self.slice*4, self.slice*2, kernel_size=3, stride=1, padding=1)),
            ('LR0', nn.LeakyReLU(negative_slope=0.2)),
            ("conv_2", nn.Conv2d(self.slice*2, self.slice*2, kernel_size=3, stride=1, padding=1)),
            ]))
        
        self.init_weight()

    def init_weight(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.xavier_normal_(m.weight)
                nn.init.constant_(m.bias, 0.01)

        # print("set xavier_uniform for nn.Conv2d.")

    def forward(self, y, para):

        y0, y1, y2, y3 = torch.chunk(y, 4, dim=1)
        decoded_y = []
        decoded_mu = []

        aux_info0 = self.aux_info_net0(para)
        y0_hat, mu_slices = self.SSC_0(y0, aux_info0)
        decoded_y.append(y0_hat)
        decoded_mu.append(mu_slices)
        
        aux_info1 = self.aux_info_net1(torch.cat([para]+decoded_y, axis=1))
        y1_hat, mu_slices = self.SSC_1(y1, aux_info1)
        decoded_y.append(y1_hat)
        decoded_mu.append(mu_slices)

        aux_info2 = self.aux_info_net2(torch.cat([para]+decoded_y, axis=1))
        y2_hat, mu_slices = self.SSC_2(y2, aux_info2)
        decoded_y.append(y2_hat)
        decoded_mu.append(mu_slices)

        aux_info3 = self.aux_info_net3(torch.cat([para]+decoded_y, axis=1))
        y3_hat, mu_slices = self.SSC_3(y3, aux_info3)
        decoded_y.append(y3_hat)
        decoded_mu.append(mu_slices)

        y_hat = torch.cat(decoded_y, 1)
        mu = torch.cat(decoded_mu, 1)

        return y_hat, mu
    
    def encode(self, y, para, scales_hat, filterCoeffs):

        y0, y1, y2, y3 = torch.chunk(y, 4, dim=1)
        s0, s1, s2, s3 = torch.chunk(scales_hat, 4, dim=1)
        decoded_y = []
        decoded_mu = []
        decode_w = []

        aux_info0 = self.aux_info_net0(para)
        y0_hat, w0_hat, mu0_slices = self.SSC_0.encode(y0, aux_info0, s0, filterCoeffs)
        decoded_y.append(y0_hat)
        decode_w.append(w0_hat)
        decoded_mu.append(mu0_slices)

        aux_info1 = self.aux_info_net1(torch.cat([para]+decoded_y, axis=1))
        y1_hat, w1_hat, mu1_slices = self.SSC_1.encode(y1, aux_info1, s1, filterCoeffs)
        decoded_y.append(y1_hat)
        decode_w.append(w1_hat)
        decoded_mu.append(mu1_slices)

        aux_info2 = self.aux_info_net2(torch.cat([para]+decoded_y, axis=1))
        y2_hat, w2_hat, mu2_slices = self.SSC_2.encode(y2, aux_info2, s2, filterCoeffs)
        decoded_y.append(y2_hat)
        decode_w.append(w2_hat)
        decoded_mu.append(mu2_slices)

        aux_info3 = self.aux_info_net3(torch.cat([para]+decoded_y, axis=1))
        y3_hat, w3_hat, mu3_slices = self.SSC_3.encode(y3, aux_info3, s3, filterCoeffs)
        decoded_y.append(y3_hat)
        decode_w.append(w3_hat)
        decoded_mu.append(mu3_slices)

        y_hat = torch.cat(decoded_y, 1)
        w_hat = torch.cat(decode_w, 1)
        mu = torch.cat(decoded_mu, 1)

        return y_hat, w_hat, mu
    
    
    def decode(self, w_hat, para):

        w0_hat, w1_hat, w2_hat, w3_hat = torch.chunk(w_hat, 4, dim=1)
        decoded_y = []
        decoded_mu = []

        aux_info0 = self.aux_info_net0(para)
        y0_hat, mu_slices = self.SSC_0.decode(w0_hat, aux_info0)
        decoded_y.append(y0_hat)
        decoded_mu.append(mu_slices)
        
        aux_info1 = self.aux_info_net1(torch.cat([para]+decoded_y, axis=1))
        y1_hat, mu_slices = self.SSC_1.decode(w1_hat, aux_info1)
        decoded_y.append(y1_hat)
        decoded_mu.append(mu_slices)

        aux_info2 = self.aux_info_net2(torch.cat([para]+decoded_y, axis=1))
        y2_hat, mu_slices = self.SSC_2.decode(w2_hat, aux_info2)
        decoded_y.append(y2_hat)
        decoded_mu.append(mu_slices)

        aux_info3 = self.aux_info_net3(torch.cat([para]+decoded_y, axis=1))
        y3_hat, mu_slices = self.SSC_3.decode(w3_hat, aux_info3)
        decoded_y.append(y3_hat)
        decoded_mu.append(mu_slices)

        y_hat = torch.cat(decoded_y, 1)
        mu = torch.cat(decoded_mu, 1)

        return y_hat, mu