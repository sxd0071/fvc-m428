import torch
import torch.nn as nn

from Common.models.NIC_models.model import Enc


class NIC_ForwardTrans(nn.Module):
    def __init__(self, encoder, **kwargs):
        super().__init__()
        # device = 'cpu' if kwargs.get("device") == None else kwargs.get("device")
        # model_id = header.model_id
        # if model_id == 0:
        #     header.M, header.N2 = 192, 128
        # if model_id == 1:
        #     header.M, header.N2 = 256, 192
        # if model_id == 2:
        #     header.M, header.N2 = 192, 128
        # if model_id > 2:
        #     raise NotImplementedError
        
        # input_features = 3
        # N1 = header.M
        # M1 = header.M // 2
        # if header.model_id < 2:
        #     header.USE_VR_MODEL = 1
        # self.encoder = Enc(input_features, N1, header.N2, header.M, M1, header.USE_VR_MODEL, header.multi_hyper_flag)
        self.encoder = encoder
        
    @torch.no_grad()
    def encode(self, im_slice, header):
        if header.coding_tool.multi_hyper_flag:
            y_main, y_hyper, y_hyper_2 = self.encoder(im_slice, lambda_rd = header.lambda_rd)
            return y_main, y_hyper, y_hyper_2
        else:
            y_main, y_hyper = self.encoder(im_slice, lambda_rd = header.lambda_rd)
            return y_main, y_hyper
