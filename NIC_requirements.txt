torch==1.11.0
torchvision==0.12.0
Pillow==9.0.1
numpy==1.21.2
scipy==1.9.2
